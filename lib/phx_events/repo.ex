defmodule PhxEvents.Repo do
  use Ecto.Repo,
    otp_app: :phx_events,
    adapter: Ecto.Adapters.Postgres
end
