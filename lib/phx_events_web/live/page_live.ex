defmodule PhxEventsWeb.PageLive do
  use Phoenix.LiveView
  alias PhxEventsWeb.PageView

  def render(assigns) do
    PageView.render("index.html", assigns)
  end

  def mount(_params,_session, socket) do
    {:ok, assign(socket, %{
      view_number: 0
    })}
  end

  def handle_event("incr",_,socket) do
    {:noreply, assign(socket, %{
      view_number: "helllo"
    })}
  end
end
