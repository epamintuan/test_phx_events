defmodule PhxEventsWeb.PageController do
  use PhxEventsWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
