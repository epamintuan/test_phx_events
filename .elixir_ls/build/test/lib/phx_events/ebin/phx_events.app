{application,phx_events,
             [{applications,[kernel,stdlib,elixir,logger,runtime_tools,
                             gettext,jason,floki,phoenix_pubsub,postgrex,
                             ecto_sql,phoenix_html,plug_cowboy,phoenix,
                             phoenix_live_view,phoenix_ecto]},
              {description,"phx_events"},
              {modules,['Elixir.PhxEvents','Elixir.PhxEvents.Application',
                        'Elixir.PhxEvents.DataCase','Elixir.PhxEvents.Repo',
                        'Elixir.PhxEventsWeb',
                        'Elixir.PhxEventsWeb.ChannelCase',
                        'Elixir.PhxEventsWeb.ConnCase',
                        'Elixir.PhxEventsWeb.Endpoint',
                        'Elixir.PhxEventsWeb.ErrorHelpers',
                        'Elixir.PhxEventsWeb.ErrorView',
                        'Elixir.PhxEventsWeb.Gettext',
                        'Elixir.PhxEventsWeb.LayoutView',
                        'Elixir.PhxEventsWeb.PageController',
                        'Elixir.PhxEventsWeb.PageLive',
                        'Elixir.PhxEventsWeb.PageView',
                        'Elixir.PhxEventsWeb.Router',
                        'Elixir.PhxEventsWeb.Router.Helpers',
                        'Elixir.PhxEventsWeb.UserSocket']},
              {registered,[]},
              {vsn,"0.1.0"},
              {mod,{'Elixir.PhxEvents.Application',[]}}]}.
